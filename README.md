# instantCI

mirror sync service for instantOS

Needed environment variables

``` 
SURGEMAIL # surge.sh account email
SURGEPASS # token found in netrc
INSTANTOSNAME # surge site prefix

NETLIFY_AUTH_TOKEN # netlify authentification
NETLIFYID # id of site to deploy to

FIREBASE_TOKEN # firenase login

VERCELTOKEN # vercel login
```
